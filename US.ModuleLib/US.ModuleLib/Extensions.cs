﻿using Rocket.Unturned.Player;
using SDG.Framework.Utilities;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using US.ModuleLib.Structs;

namespace US.ModuleLib
{
    public static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> src, Action<T> action)
        {
            foreach (var obj in src)
            {
                action(obj);
            }
        }

        public static SVector3 SVector3(this Vector3 pos)
        {
            return new SVector3() { x = pos.x, y = pos.y, z = pos.z };
        }

        public static UnturnedPlayer UPlayer(this SteamPlayer player)
        {
            return UnturnedPlayer.FromSteamPlayer(player);
        }

        public static bool IsUInt16(this string str)
        {
            return ushort.TryParse(str, out ushort shit);
        }

        public static ushort ToUInt16(this string str)
        {
            return ushort.Parse(str);
        }

        public static byte ToByte(this string str)
        {
            return byte.Parse(str);
        }

        public static bool IsByte(this string str)
        {
            return byte.TryParse(str, out byte s);
        }

        public static void AddMyOwnComponent(this UnturnedPlayer player, Type component)
        {
            player.Player.gameObject.AddComponent(component);
        }

        public static bool ProperlyGiveItem(this UnturnedPlayer player, Item item)
        {
            bool drop = player.Player.inventory.tryAddItem(item, true);
            if (!drop)
                ItemManager.dropItem(item, player.Position, true, Dedicator.isDedicated, true);
            return drop;
        }

        public static CSteamID ToSteamID(this string str)
        {
            return new CSteamID(Convert.ToUInt64(str));
        }

        public static string GetItemName(this ushort id)
        {
            return ((ItemAsset)Assets.find(EAssetType.ITEM, id)).itemName;
        }

        public static ItemAsset GetItemAsset(this ushort id)
        {
            return (ItemAsset)Assets.find(EAssetType.ITEM, id);
        }

        public static ItemAsset GetItemAsset(this string name)
        {
            Asset[] array = Assets.find(EAssetType.ITEM);
            for (int i = 0; i < array.Length; i++)
            {
                ItemAsset vAsset = (ItemAsset)array[i];
                if (vAsset != null && vAsset.name != null && vAsset.name.ToLower().Contains(name.ToLower()))
                {
                    return vAsset;
                }
            }
            return null;
        }

        public static Item GetHat(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.hat, 1, player.Player.clothing.hatQuality, player.Player.clothing.hatState);
        }

        public static Item GetBackPack(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.backpack, 1, player.Player.clothing.backpackQuality, player.Player.clothing.backpackState);
        }

        public static Item GetGlasses(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.glasses, 1, player.Player.clothing.glassesQuality, player.Player.clothing.glassesState);
        }

        public static Item GetMask(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.mask, 1, player.Player.clothing.maskQuality, player.Player.clothing.maskState);
        }

        public static Item GetPants(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.pants, 1, player.Player.clothing.pantsQuality, player.Player.clothing.pantsState);
        }

        public static Item GetShirt(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.shirt, 1, player.Player.clothing.shirtQuality, player.Player.clothing.shirtState);
        }

        public static Item GetVest(this UnturnedPlayer player)
        {
            return new Item(player.Player.clothing.vest, 1, player.Player.clothing.vestQuality, player.Player.clothing.vestState);
        }

        public static Asset ToAsset(this Item item)
        {
            return GetItemAsset(item.id);
        }

        public static Item[] GetAllClothing(this UnturnedPlayer player)
        {
            Item[] ass =
            {
                player.GetBackPack(),
                player.GetVest(),
                player.GetGlasses(),
                player.GetHat(),
                player.GetMask(),
                player.GetPants(),
                player.GetShirt()
            };

            return ass;
        }

        public static Item[] GetAllItems(this UnturnedPlayer player)
        {
            List<Item> Items = new List<Item>();
            var inventory = player.Player.inventory;
            for (byte i = 0; i < 7; i++)
            {
                var count = inventory.getItemCount(i);
                for (byte j = 0; j < count; j++)
                {
                    var item = inventory.getItem(i, j).item;
                    if (item != null) Items.Add(item);
                }
            }
            return Items.ToArray();
        }

        public static Asset[] ToAssets(this Item[] array)
        {
            List<Asset> ass = new List<Asset>();
            foreach (var v in array)
            {
                ass.Add(GetItemAsset(v.id));
            }
            return ass.ToArray();
        }

        public static void Freeze(this UnturnedPlayer player)
        {
            player.Player.movement.sendPluginSpeedMultiplier(0);
        }

        public static void UnFreeze(this UnturnedPlayer player)
        {
            player.Player.movement.sendPluginSpeedMultiplier(1);
        }

        public static Vector3 LookingAt(this UnturnedPlayer player, int raymask)
        {
            PlayerLook look = player.Player.look;
            PhysicsUtility.raycast(new Ray(look.aim.position, look.aim.forward), out RaycastHit hit, float.PositiveInfinity, raymask, QueryTriggerInteraction.UseGlobal);
            return hit.point;
        }
    }
}
