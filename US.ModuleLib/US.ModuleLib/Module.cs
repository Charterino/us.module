﻿using Rocket.API;
using Rocket.API.Serialisation;
using Rocket.Core;
using Rocket.Core.Utils;
using Rocket.Unturned.Events;
using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using UnityEngine;
using US.ModuleLib.Patching;
using US.ModuleLib.Patching.PatchInfo;
using Environment = Rocket.Core.Environment;
using RLog = Rocket.Core.Logging.Logger;

namespace US.ModuleLib
{
    public class Module
    {
        public static Module module;
        private List<Type> components;
        private List<Type> bases;
        public string ConfigurationPath;

        //delegates
        public delegate void VehicleAdded();
        public delegate void GrenadeUsed(Transform transform);

        //events
        public event VehicleAdded OnVehicleAdded;
        public event GrenadeUsed OnGrenadeUsed;

        //methods for events
        public void InvokeOnVehicleAdded() { OnVehicleAdded?.Invoke(); }
        public void InvokeOnGrenadeUsed(Transform transform) { OnGrenadeUsed?.Invoke(transform); }

        public void Start()
        {
            R.OnRockedInitialized += R_OnRockedInitialized;
            Console.WriteLine("Starting the US.Module...");
            module = this;
            components = new List<Type>();
            bases = new List<Type>();
            Provider.onEnemyConnected += Events_OnBeforePlayerConnected;
            //Patch();

            GameObject obj = new GameObject("US.Module");
            obj.AddComponent<BehaviourClass>();
        }


        private void Patch()
        {
            new TypeForPatch(typeof(VehicleManager), null).GetMethod("addVehicle").Postfix(typeof(VehicleManagerPatcher), "Postfix").Save();
            new TypeForPatch(typeof(UseableThrowable), null).GetMethod("toss").Postfix(typeof(UseableThrowablePatcher), "PostFix").Save();
        }

        private void R_OnRockedInitialized()
        {
            PlayerInput.onPluginKeyTick += KeyUpdated;
        }

        public void KeyUpdated(Player player, uint simulation, byte key, bool state)
        {
            /*
                < - 0
                > - 1
                ? - 2
                : - 3
                " - 4
             */
            if (state)
            {
                if (key == 2)
                    player.serversideSetPluginModal(!player.inPluginModal);
                
            }
        }

        

        public int GetRandomNumber(int max)
        {
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            return int.Parse(new WebClient().DownloadString("https://www.random.org/integers/?num=1&min=1&max=" + max +"&col=1&base=10&format=plain&rnd=new"));
        }

        
        public bool MyRemoteCertificateValidationCallback(System.Object sender,
    X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }


        private void Events_OnBeforePlayerConnected(SteamPlayer player)
        {
            foreach (var v in components)
            {
                player.UPlayer().AddMyOwnComponent(v);
            }
        }

        /// <summary>
        /// Gets prefix of the player
        /// </summary>
        public string GetPrefix(CSteamID id)
        {
            List<RocketPermissionsGroup> groups = R.Permissions.GetGroups(new RocketPlayer(id.ToString()), true);
            string pref = "";
            short pre = 0;
            for (int i = 0; i < groups.Count; i++)
            {
                RocketPermissionsGroup v = groups[i];
                if (i == 0)
                {
                    pref = v.Prefix;
                    pre = v.Priority;
                }
                else
                {
                    if (v.Priority < pre)
                    {
                        pre = v.Priority;
                        pref = v.Prefix;
                    }
                }
            }
            return pref;
        }


        /// <summary>
        /// Gets suffix of the player
        /// </summary>
        public string GetSuffix(CSteamID id)
        {
            List<RocketPermissionsGroup> groups = R.Permissions.GetGroups(new RocketPlayer(id.ToString()), true);
            string pref = "";
            short pre = 0;
            for (int i = 0; i < groups.Count; i++)
            {
                RocketPermissionsGroup v = groups[i];
                if (i == 0)
                {
                    pref = v.Suffix;
                    pre = v.Priority;
                }
                else
                {
                    if (v.Priority < pre)
                    {
                        pre = v.Priority;
                        pref = v.Suffix;
                    }
                }
            }
            return pref;
        }

        public void RegisterComponent(MyOwnComponent component)
        {
            components.Add(component.GetType());
        }
    }
}
