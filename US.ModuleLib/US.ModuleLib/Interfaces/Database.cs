﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UnityEngine;
using Logger = Rocket.Core.Logging.Logger;

namespace US.ModuleLib.Interfaces
{
    public abstract class Database<T> : ICLoadable
    {
        public Database()
        {
            Name = GetType().Name;
            Load();
        }

        ~Database()
        {
            LiteDatabase.Dispose();
        }

        public string Name { get; set; } 
        public string Path;
        public virtual LiteCollection<T> Base { get; set; }
        public LiteDatabase LiteDatabase { get; set; }

        public event ItemInserted OnItemInserted;
        public event ItemDeleted OnItemDeleted;
        public event ItemUpdated OnItemUpdated;

        public void Load()
        {
            Logger.Log("Loading LiteDB for " + Name);
            Path = Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + "Database" + System.IO.Path.DirectorySeparatorChar + Name + ".db";
            Logger.Log(Path);
            if (!Directory.Exists(Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + "Database"))
            {
                Directory.CreateDirectory((Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + "Database"));
            }
            LiteDatabase = new LiteDatabase(Path);
            Base = LiteDatabase.GetCollection<T>(Name);
            Logger.Log("LiteDB loaded.");
        }

        public virtual void Insert(T item)
        {
            Base.Insert(item);
            OnItemInserted?.Invoke(item);
        }

        public virtual void Update(T item)
        {
            Base.Update(item);
            OnItemUpdated?.Invoke(item);
        }

        public virtual void Delete(Expression<Func<T, bool>> t)
        {
            Base.Delete(t);
            OnItemDeleted?.Invoke(FindOne(t));
        }

        public virtual List<T> Find(Expression<Func<T, bool>> t)
        {
            return Base.Find(t).ToList();
        }

        public virtual T FindOne(Expression<Func<T, bool>> t)
        {
            return Base.FindOne(t);
        }

        public int Count => Base.Count();

        public delegate void ItemInserted(T item);
        public delegate void ItemUpdated(T item);
        public delegate void ItemDeleted(T item);
    }
}
