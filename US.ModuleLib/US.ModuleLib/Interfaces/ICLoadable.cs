﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.ModuleLib.Interfaces
{
    public interface ICLoadable
    {
        void Load();
    }
}
