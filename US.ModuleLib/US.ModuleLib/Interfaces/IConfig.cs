﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.ModuleLib.Interfaces
{
    public interface IConfig
    {
        string FileName { get; }
        void LoadDefaults();
        void Load(string filePath);
        void Save(string filePath);
    }
}
