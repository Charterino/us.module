﻿using Rocket.API;
using Rocket.Core.Assets;
using Rocket.Core.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Environment = Rocket.Core.Environment;

namespace US.ModuleLib.Interfaces
{
    public class XmlAddict<T> where T : class, IDefaultable
    {
        public XmlAddict()
        {
            Load();
        }


        public IAsset<T> Base;

        public void Load()
        {
            Logger.Log("Loading XMLAddict for " + typeof(T).Name);
            
            string path = Path.Combine(Environment.PluginsDirectory, typeof(T).Assembly.GetName().Name, typeof(T).Name) + ".xml";
            Base = new XMLFileAsset<T>(path, null, default);
            Logger.Log("Done");
        }
    }
}
