﻿using Rocket.Unturned.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace US.ModuleLib
{
    public class MyOwnComponent : MonoBehaviour
    {
        private void Awake()
        {
            Player = UnturnedPlayer.FromPlayer(base.gameObject.transform.GetComponent<SDG.Unturned.Player>());
            DontDestroyOnLoad(transform.gameObject);
        }

        private void OnEnable()
        {
            Load();
        }

        private void OnDisable()
        {
            Unload();
        }

        protected virtual void Load() { }
        protected virtual void Unload() { }
        public UnturnedPlayer Player;
    }
}
