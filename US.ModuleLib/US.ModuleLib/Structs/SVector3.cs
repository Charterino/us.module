﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace US.ModuleLib.Structs
{
    public struct SVector3
    {
        public float x { get; set; }
        public float y { get; set; }
        public float z { get; set; }

        public Vector3 ToVector3()
        {
            return new Vector3(x, y, z);
        }

        public static bool operator == (SVector3 left, SVector3 right)
        {
            return !(left != right);
        }

        public static bool operator != (SVector3 left, SVector3 right)
        {
            if (left.x != right.x || left.y != right.y || left.z != right.z) return true;
            else return false;
        }
    }
}
