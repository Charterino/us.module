﻿using SDG.Unturned;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace US.ModuleLib
{
    public static class SteamCall
    {
        private static SteamChannel Channel { get => LevelManager.instance.channel; }

        public static void AirdropState(Vector3 state, Vector3 direction, float speed, float force, float delay, ESteamCall mode = ESteamCall.CLIENTS)
        {
            Channel.send("tellAirdropState", mode, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
            {
                    state,
                    direction,
                    speed,
                    force,
                    delay
            });
        }

        public static void AirdropState(CSteamID target, Vector3 state, Vector3 direction, float speed, float force, float delay)
        {
            Channel.send("tellAirdropState", target, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
            {
                    state,
                    direction,
                    speed,
                    force,
                    delay
            });
        }
    }
}
