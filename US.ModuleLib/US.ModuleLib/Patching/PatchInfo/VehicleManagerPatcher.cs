﻿using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace US.ModuleLib.Patching.PatchInfo
{
    public class VehicleManagerPatcher
    {
        internal static void Postfix()
        {
            Module.module.InvokeOnVehicleAdded();
        }
    }
}
