﻿using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace US.ModuleLib.Patching
{
    public class TypeForPatch
    {
        private readonly Harmony instance;
        private readonly Type type;
        public TypeForPatch(Type type, Harmony instance)
        {
            this.type = type;
            this.instance = instance;
        }

        public Patch GetMethod(MethodInfo info)
        {
            return new Patch(info.DeclaringType, instance)
            {
                PatchedMethod = info
            };
        }

        public Patch GetMethod(string name, Type[] paramTypes = null, Type[] generics = null)
        {
            MethodInfo methodInfo = AccessTools.Method(this.type, name, paramTypes, generics);
            return GetMethod(methodInfo);
        }
    }
}
