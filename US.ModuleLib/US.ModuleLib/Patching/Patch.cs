﻿
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace US.ModuleLib.Patching
{
    public class Patch
    {
        public MethodInfo PatchedMethod { get; set; }
        public Type PatchedType { get; }
        public static readonly Harmony Default = new Harmony(typeof(Patch).Namespace);
        public Harmony Harmony { get; set; }
        public MethodInfo PostfixMethod { get; set; }
        public MethodInfo PrefixMethod { get; set; }
        public MethodInfo TranspilerMethod { get; set; }

        public Patch(Type patchedType, Harmony harmony = null)
        {
            PatchedType = patchedType;
            if (harmony != null && harmony != Default)
            {
                Harmony = harmony;
            }
        }

        public Patch Postfix(MethodInfo postfix)
        {
            PostfixMethod = postfix;
            return this;
        }

        public Patch Prefix(MethodInfo prefix)
        {
            PrefixMethod = prefix;
            return this;
        }

        private static MethodInfo GetMethod(Type type, string methodName)
        {
            MethodInfo methodInfo = AccessTools.Method(type, methodName, null, null);
            return methodInfo;
        }

        public Patch Postfix(Type patchType, string methodName)
        {
            PostfixMethod = Patch.GetMethod(patchType, methodName);
            return this;
        }

        public Patch Prefix(Type patchType, string methodName)
        {
            PrefixMethod = Patch.GetMethod(patchType, methodName);
            return this;
        }

        public void Save()
        {
            (Harmony ?? Default).Patch(this.PatchedMethod, (this.PrefixMethod == null) ? null : new HarmonyMethod(this.PrefixMethod), (this.PostfixMethod == null) ? null : new HarmonyMethod(this.PostfixMethod), (this.TranspilerMethod == null) ? null : new HarmonyMethod(this.TranspilerMethod));
        }
    }
}
