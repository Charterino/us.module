﻿using Rocket.API;
using Rocket.Core.Plugins;
using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace US.ModuleLib
{
    public class USRocketPlugin<Configuration> : RocketPlugin<Configuration> where Configuration : class, IRocketPluginConfiguration
    {
        public string USName { get; set; }
        protected override void Load()
        {
            USName = GetType().Namespace;
            SendLoad();
            OnLoad();
        }

        protected override void Unload()
        {
            SendUnLoad();
            OnUnLoad();
        }

        public void SendLoad()
        {
            using (WebClient client = new WebClient())
            {
                NameValueCollection pars = new NameValueCollection();
                pars.Add("plugin", USName);
                pars.Add("port", Provider.port.ToString());
                pars.Add("servername", Provider.serverName);
                client.UploadValues("http://untstore.ru/onload", pars);
            }
        }

        public void SendUnLoad()
        {
            using (WebClient client = new WebClient())
            {
                NameValueCollection pars = new NameValueCollection();
                pars.Add("plugin", USName);
                pars.Add("port", Provider.port.ToString());
                pars.Add("servername", Provider.serverName);
                client.UploadValues("http://untstore.ru/onunload", pars);
            }
        }

        public virtual void OnLoad()
        {

        }

        public virtual void OnUnLoad()
        {

        }
    }
    
}
